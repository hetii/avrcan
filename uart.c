#include <avr/io.h>
#include "uart.h"

void init_usart( unsigned int ubrr){
   /* Set baud rate */
   UBRRH = (unsigned char)(ubrr>>8);
   UBRRL = (unsigned char)ubrr;
   /* Enable receiver and transmitter */
   UCSRB = (1<<RXEN)|(1<<TXEN);
   /* Set frame format: 8data, 2stop bit */
   UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);
} 

void send_usart_char(char c){
    UDR = c;
    loop_until_bit_is_set(UCSRA, TXC);
    UCSRA |=_BV(TXC);
}

void send_usart_int(int c){
    UDR = c;
    loop_until_bit_is_set(UCSRA, TXC);
    UCSRA |=_BV(TXC);
}

char get_usart(void){
    loop_until_bit_is_set(UCSRA, RXC);
    UCSRA |=_BV(RXC);
    return UDR;
}

void string_usart(char * text){
    while(*text){
        send_usart_char(*text++);
    }
}

void print(char * text){
    string_usart(text);
    string_usart("\n");
}
