#include <stdio.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "utils.h"
#include "mcp2515.h"
#include "uart.h"

void print_can_message(tCAN *message)
{
        uint8_t length = message->header.length;
       
        PRINT("id:     0x%3x\n", message->id);
        PRINT("len: %d\n", length);
        PRINT("rtr:    %d\n", message->header.rtr);
       
        if (!message->header.rtr) {
                PRINT("dane:  ");
              	uint8_t i; 
                for (i = 0; i < length; i++) {
                        PRINT("0x%02x ", message->data[i]);
                }
                PRINT("\n");
        }
}

void test_can_loopback(void){ 
        PRINT("Tworzenie wiadomości\n");
        tCAN message;
       
        // Niektóre wartości
        message.id = 0x123;
        message.header.rtr = 0;
        message.header.length = 2;
        message.data[0] = 0xab;
        message.data[1] = 0xcd;
       
        print_can_message(&message);
       
        PRINT("Tryb Loopback\n\n");
        mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), (1<<REQOP1));
       
        // Wyślij wiadomość
        if (mcp2515_send_message(&message)) {
                PRINT("Wisdomość zapisana w buforze\n");
        }
        else {
                PRINT("Błąd: Nie można wysłać wiadomości\n");
        }
       
        // No to chwilkę poczekamy
        _delay_ms(10);
       
        if (mcp2515_check_message()) {
                PRINT("Odebrano wiadomość!\n");
               
                // odczyt wiadomości z buforów
                if (mcp2515_get_message(&message)) {
                        print_can_message(&message);
                        PRINT("\n");
                }
                else {
                        PRINT("Błąd: Nie można odczytać wiadomości\n\n");
                }
        }
        else {
                PRINT("Błąd: Nie odebrano wiadomości\n\n");
        }
       
        PRINT("Powrót do normalnego trybu\n\n");
        mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), 0);
	// Ponowne podłączenie do magistrali CAN
        /*
        PRINT("Próba wysłania wiadomości przez CAN\n");
 
        if (mcp2515_send_message(&message)) {
                PRINT("Wiadomość została napisana w buforze\n\n");
        }
        else {
                PRINT("Błąd: Nie można wysłać wiadomości\n\n");
        }
	*/
}
