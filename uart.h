#include <avr/io.h>

#define BAUD 9600
#define MYUBRR F_OSC/16/BAUD-1 

void init_usart(unsigned int ubrr);
void send_usart_char(char c);
void send_usart_int(int c);
char get_usart(void);
void string_usart(char * text);
void print(char * text);
