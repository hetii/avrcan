#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <util/delay.h>

#include "uart.h"
#include "mcp2515.h"
#include "global.h"
#include "defaults.h"
#include "utils.h"

#define KEY_INPUT       PINC
#define LED_OUTPUT      PORTC
#define LED_DIR         DDRC
#define KEY_SWITCH      

char key_state;                         // debounced and inverted key state:
                                        // bit = 1: key pressed
char key_press;                         // key press detect

ISR(TIMER0_OVF_vect)
{
  static char ct0, ct1;
  char i;
  LED_DIR = 0;                          // as input
  i = key_state ^ ~KEY_INPUT;           // key changed ?
  ct0 = ~( ct0 & i );                   // reset or count ct0
  ct1 = ct0 ^ (ct1 & i);                // reset or count ct1
  i &= ct0 & ct1;                       // count until roll over ?
  key_state ^= i;                       // then toggle debounced state
                                        // now debouncing finished
  key_press |= key_state & i;           // 0->1: key press detect
  LED_DIR = 0xFF;                       // as output
}

char get_key_press( char key_mask )
{
  cli();
  key_mask &= key_press;                // read key(s)
  key_press ^= key_mask;                // clear key(s)
  sei();
  return key_mask;
}

static int putchar__(char c, FILE *stream) {
        send_usart_char(c);
        return 0;
}
static FILE mystdout = FDEV_SETUP_STREAM(putchar__, 0, _FDEV_SETUP_WRITE);

int main(void)
{
	init_usart(MYUBRR);
	key_state = 0;
	key_press = 0;

	TCCR0 = 1<<CS02;                      //divide by 256 * 256
	TIMSK = 1<<TOIE0;                     //enable timer interrupt

	LED_DIR = 0xFF;
	LED_OUTPUT = 0xFF;
        sei();
        stdout = &mystdout;
       
        if (!mcp2515_init()) {
                PRINT("Błąd: MCP2515 nie gotowy\n");
                for (;;);
        }
        else {
                PRINT("MCP2515 aktywny\n\n");
        }
      
	test_can_loopback();

	tCAN message;
	message.id = 0x123;
	message.header.rtr = 0;
        message.header.length = 1;
	message.data[0] = KEY_INPUT;
 
        if (mcp2515_send_message(&message)) {
                PRINT("Wiadomość została napisana w buforze\n\n");
        } else {
                PRINT("Błąd: Nie można wysłać wiadomości\n\n");
        }

	PRINT("Oczekiwanie na odebranie wiadomości\n\n");

	uint8_t old_KEY_INPUT = PINC;

        while (1) {
		LED_OUTPUT ^= get_key_press( 0xFF );

		if (old_KEY_INPUT != LED_OUTPUT){
			old_KEY_INPUT = LED_OUTPUT;
			message.data[0] = LED_OUTPUT;
			mcp2515_send_message(&message);
		}

                // Czekaj, aż pojawi się wiadomość
                if (mcp2515_check_message()) {
                        PRINT("Odebrano wiadomość!\n");
                       
                        //  Czytanie wiadomości z buforów MCP2515
                        if (mcp2515_get_message(&message)) {
                                print_can_message(&message);
                                PRINT("\n");
				LED_OUTPUT = message.data[0];
                        }
                        else {
                                PRINT("Nie można odczytać wiadomości\n\n");
                        }
               }
        }
       
        return 0;
}
